#!/bin/bash

cd "$(dirname $0)"

while : ; do
    read -p "Remove vagrant generated files (y/n)? " yn
    case "${yn}" in
        [yY]*|[Oo]* )
	    rm -f Vagrantfile	    
	    break;;
	""|[Nn]* ) break;;
        * ) echo "Please answer y/n.";;
    esac
done

while : ; do
    read -p "Remove proxy generated files (y/n)? " yn
    case "${yn}" in
        [yY]*|[Oo]* )
	    rm -f files/kaz/.apt-mirror-config files/kaz/.proxy-config
	    rm -f files/.apt-mirror-config files/.proxy-config files/.docker-config.json
	    break;;
	""|[Nn]* ) break;;
        * ) echo "Please answer y/n.";;
    esac
done

while : ; do
    read -p "Remove custom generated files (y/n)? " yn
    case "${yn}" in
        [yY]*|[Oo]* )
	    rm -f files/.customDocker.sh
	    rm -f files/kaz/.customDocker.sh files/local.sh
	    break;;
	""|[Nn]* ) break;;
        * ) echo "Please answer y/n.";;
    esac
done
