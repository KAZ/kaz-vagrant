#!/usr/bin/env bash

set -e

# Get HD UUID
HDUUID=`vboxmanage showvminfo kaz-vm --machinereadable | grep ImageUUID | cut -d= -f2 | sed -e "s/\"//g"`

# Get storage controller
STCTRL=`vboxmanage showvminfo kaz-vm --machinereadable | grep storagecontrollername0 | cut -d= -f2 | sed -e "s/\"//g"`

#echo -e $HDUUID
#echo -e $STCTRL

vboxmanage storageattach kaz-vm --medium="$HDUUID" --storagectl="${STCTRL}" --port=0 --discard=on --nonrotational=on

echo "Trim enabled !"
