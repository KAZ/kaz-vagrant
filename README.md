# kaz-vagrant

[Kaz](https://kaz.bzh/) est un CHATONS du Morbihan. Nous proposons ici un moyen de le répliquer dans une VM. Il y a des éléments de configuration à définir avant d'initialiser ce simulateur. 
Le principe est de faire fonctionner un simulateur de notre CHATONS dans une VirtualBox pour mettre au point nos différents services.

Nous utilisons :
  * Vagrant pour automatiser la création de la Machine Virtuelle
  * VirtualBox pour une VM isolée
  * [SNSTER](https://framagit.org/flesueur/snster) pour créer des services internet tiers et notre serveur
  * LXC pour faire tourner ces services dans des conteneurs distincts (ie, kaz-prod est un conteneur LXC)
  * Docker pour chaque service de notre serveur

À la fin, nous obtenons une maquette d'un petit internet simulé, avec du DNS, des mails tiers, et nos serveurs hoster-a-kaz1 et hoster-b-kaz2 dans un coin.

![topologie](/doc/images/topologie.png)


## Pré-requis

Vous avez besoin de [vagrant](https://www.vagrantup.com/), [VirtualBox](https://www.virtualbox.org/) et éventuellement git.


## Installation

* Télécharger le dépôt kaz-vagrant ou utilisez la commande git :
```bash
git clone https://git.kaz.bzh/KAZ/kaz-vagrant.git # pour essayer
git clone git+ssh://git@git.kaz.bzh:2202/KAZ/kaz-vagrant.git # pour contribuer
cd kaz-vagrant/
```
* (Optionnel) Ajustez éventuellement la mémoire et les cpus utilisés dans Vagrantfile (par défaut 4GB et 2 vCPUs)

* Pour créer tout l'univers Kaz il faut se placer dans le répertoire et lancer la commande :
```bash
vagrant up
```

Cette étape peut-être (très) longue, notamment la construction des machines Kaz... Comptez entre 40 minutes et quelques heures, selon la connexion réseau et les performances de la machine.



## Utilisation

Les utilisateurs créés sont
  * debian/debian
  * root/root.

Se connecter en root/root

Lors du démarrage de la VM, il faut lancer SNSTER :
```bash
cd /root/snster-kaz
snster start
```

Normalement, hoster-a-kaz1 et hoster-b-kaz2 lancent automatiquement les dockers (dans rc.local), mais si ça ne marche pas bien il peut falloir les relancer (que se passe-t-il si on relance container.sh pendant que container.sh n'est pas encore fini ? faut-il l'enlever du rc.local ? Le lancement initial peut rater, probablement si le DNS n'est pas encore fonctionnel lors du lancement, à mettre au point et peut-être enlever du rc.local ?)
```bash
snster attach hoster-a-kaz1 -x /kaz/bin/container.sh start
snster attach hoster-b-kaz2 -x /kaz/bin/container.sh start
```

Vous pouvez alors (toutes les commandes snster doivent être exécutées dans `/root/snster-kaz`) :
* Afficher un bureau graphique sur une machine tierce à Kaz : `snster display isp-a-home`. Sur cette machine, vous pouvez :
  * Ouvrir Firefox et naviguer vers :
    * `https://www.kaz.sns`, le Kaz interne à la VM
    * `https://listes.kaz.sns`, le sympa interne à la VM
    * `https://pad2.kaz.sns`, le pad sur kaz2
    * `https://www.kaz.bzh`, le vrai Kaz
  * Ouvrir claws-mail et retrouver les comptes mails configurés :
    * `contact1@kaz.sns` à `contact4@kaz.sns`, hébergés sur le kaz-prod de la VM
    * `email@isp-a.sns`, hébergé dans le conteneur LXC isp-a-infra
* Travailler sur hoster-a-kaz1 : `snster attach hoster-a-kaz1`
* Afficher un plan de réseau : `snster print`
* Le système de fichiers de hoster-a-kaz1 est accessible directement dans la VM:
  * `/kaz1-prod/` [VM] correspond à `/` [hoster-a-kaz1]
  * `/kaz` [VM] correspond à `/kaz` [hoster-a-kaz1]
* Il est probablement pratique d'installer son environnement de développement sur la VM, avec ses clés SSH et son éditeur favori.

Dans Vagrantfile, existent des répertoires partagés entre le host et le VM mais pour qu'ils soient visibles depuis la VM, il faut démarrer la VM non pas avec VirtualBox mais avec Vagrant up.

Il y a un aperçu de l'état des services avec l'url https://kaz.sns/status/allServices.html

![status](/doc/images/allServices.jpg)

Les erreurs 502 correspondent à des fonctions en cours de développement. Les message "Can't open this page" correspond au fait que le services refuse pour des raison de sécurité de de fonctionner embarqué dans une page.


Vous pouvez également démarrer firefox avec les URL suivantes:
  * https://www.kaz.sns
  * https://tableur.kaz.sns
  * https://pad.kaz.sns
  * https://depot.kaz.sns
  * https://agora.kaz.sns/login (compte contact1@kaz.local créé, mot de passe toto)
  * https://cloud.kaz.sns/login (compte contact1@kaz.local créé, mot de passe totototototototo1234 )
  * https://sondage.kaz.sns

Il vous faudra accepter les éventuelles alertes de sécurité pour certificat absent (web et messagerie)

## Mise au point

Pour réinstaller Kaz sur kaz1 (avec suppression de /kaz, des volumes dockers et réinstallation complète; idem kaz2), depuis la VM :
```bash
snster attach hoster-a-kaz1 -x "/root/kaz.sh"
```

Pour détruire la VM et recommencer, depuis l'hôte :
```bash
vagrant destroy
vagrant up
```

Accélération de la construction avec un proxy cache local
---------------------------------------------------------

Au tout début de la construction de la VM, un proxy Squid et un proxy Dockerhub (docker-registry) sont installés au niveau de la VM. Ils font du cache et sont ensuite utilisé lors des apt-get du provisionning de la VM puis lors des constructions des conteneurs LXC et des dockers. Quelques téléchargements ne sont pas encore mis en cache (soit parce que certains téléchargements se font hors de ce proxy, soit par l'utilisation du HTTPS qui n'est pas (encore) intercepté pour faire ce cache), mais cela diminue déjà beaucoup le trafic réseau lors de la construction et lors des reconstructions partielles ensuite.

Il est possible de configurer ce proxy pour utiliser un proxy du réseau local à son tour. L'intérêt est d'avoir un cache persistant lors de la reconstruction de la VM, ou de pouvoir rediriger certaines requêtes (dépôts Debian ou Alpine) vers des miroirs locaux. L'installation et la configuration sont décrites dans le fichier [CACHING.md](CACHING.md)
