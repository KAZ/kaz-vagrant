# Utilisation d'un cache local

Il est possible de configurer la VM pour utiliser un proxy et un registre docker du réseau local. L'intérêt est d'avoir un cache persistant lors de la reconstruction de la VM, ou de pouvoir rediriger certaines requêtes (dépôts Debian ou Alpine) vers des miroirs locaux.

## Configuration de la VM

Dans le dossier vagrant du host, il faut un fichier `files/customVM.sh`. Un fichier `files/customVM.sh.dist` est fourni en exemple : il suffit de le renommer en `customVM.sh`, puis de modifier les IP du proxy et du registre Docker upstreams dans les premières lignes en remplaçant par son IP privée sur le lan. ```hostname -I``` pour connaître son ip privée.

Il est évidemment possible de n'activer que l'une des 2 fonctionnalités (soit que le proxy http externe, soit que le docker registry externe) en commentant les lignes associées. D'autres modifications de la VM peuvent aussi être réalisées dans ce fichier...

## Installation d'un proxy squid sur l'hôte

Pour installer un squid sur l'hôte :
* `apt install squid`
* Éditer `/etc/squid/squid.conf` :
  * Décommenter la ligne `#http_access allow localnet` (ATTENTION ! Il faut bien décommenter cette ligne existante, et non l'ajouter à la fin, sa place dans le fichier de conf est importante). Ne pas confondre `allow localnet` et `allow localhost`.
  * Ajouter à la fin :
```
cache_dir aufs /var/spool/squid 5000 14 256
maximum_object_size 4000 MB
http_port 3142
```

penser à redémarrer le squid: `service squid restart`

## Installation d'un docker-registry sur l'hôte

Pour installer un docker-registry sur l'hôte :
* `apt install docker-registry`
* Éditer `/etc/docker/registry/config.yml` :
  * Enlever la section `auth`
  * Ajouter à la fin :
```yaml
proxy:
  remoteurl: https://registry-1.docker.io
```

penser à redémarrer: `service docker-registry restart`

Configurer une IP pérenne
-------------------------

Si vous utilisez un ordinateur portable, votre adresse IP changera sûrement au grè de vos connexions à l'Internet. Dans ce cas il est préférable d'utiliser une adresse stable pour designer votre mandataire.

Il est par exemple possible d'assigner une seconde adresse à la carte loopback de votre hôte avec `/sbin/ifconfig lo:0 192.168.128.1/24`
