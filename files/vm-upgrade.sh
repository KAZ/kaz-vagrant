#!/bin/bash
# Upgrade de tout sauf kaz-prod

if [ -z "${KAZGUARD}" ] ; then
    exit 1
fi
set -e

# On met à jour SNSTER
cd /root/snster
git switch main
git pull
./install.sh
# hotfix pour btrfs
sed -i -e "s/template=self.template/template=self.template, bdevtype='btrfs'/" /usr/local/lib/python3.9/dist-packages/backends/LxcBackend.py

# On récupère le dernier kaz-vagrant
if [ -z "${KAZBRANCH}" ] ; then
   KAZBRANCH="master"
fi
cd /tmp
git clone https://git.kaz.bzh/KAZ/kaz-vagrant.git || (cd kaz-vagrant && git fetch && git switch "${KAZBRANCH}" && git pull)
cd /tmp/kaz-vagrant
git switch "${KAZBRANCH}"

# On écrase les anciens fichiers
cp -ar /tmp/kaz-vagrant/files/snster-kaz /root/
# crypto keys
cp -ar /root/tls /root/snster-kaz/hoster-a/kaz1/
cp -ar /root/tls /root/snster-kaz/hoster-b/kaz2/
cp -ar /root/tls /root/snster-kaz/isp-a/home/
cp -ar /root/tls /root/snster-kaz/mica/infra/

# On détruit et reconstruit tout sauf kaz-prod
SNSTER="snster -c /root/snster-kaz"
$SNSTER destroy isp-a-home
$SNSTER destroy isp-a-infra
$SNSTER destroy isp-a-router
$SNSTER destroy hoster-a-router
$SNSTER destroy hoster-b-router
$SNSTER destroy mica-router
$SNSTER destroy mica-infra
$SNSTER destroy opendns-router
$SNSTER destroy opendns-resolver
$SNSTER destroy root-p-router
$SNSTER destroy root-p-rootns
$SNSTER destroy tld-sns-router
$SNSTER destroy tld-sns-ns
$SNSTER destroy transit-a-router

$SNSTER create

$SNSTER start
