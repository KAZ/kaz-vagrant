#!/bin/bash
# Configure claws mail
set -e
#if [ -z $KAZGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

# $domain, $mailname, $password

name=$1
domainname=$2
login=$3
password=$4
number=$5

# cp -ar claws-mail ~/.claws-mail

rm /tmp/accountrc /tmp/folderlist.xml -f

mkdir -p ~/.claws-mail

cat claws-mail/accountrc > /tmp/accountrc
sed -i -e "s/\$name/$name/" /tmp/accountrc
sed -i -e "s/\$domainname/$domainname/" /tmp/accountrc
sed -i -e "s/\$number/$number/" /tmp/accountrc
sed -i -e "s/\$login/$login/" /tmp/accountrc
cat /tmp/accountrc >> ~/.claws-mail/accountrc

if [ ! -f ~/.claws-mail/folderlist.xml ]; then
    cp claws-mail/folderlist_skel.xml ~/.claws-mail/folderlist.xml
fi

cat claws-mail/folderlist.xml > /tmp/folderlist.xml
sed -i -e "s/\$name/$name/" /tmp/folderlist.xml
sed -i -e "s/\$domainname/$domainname/" /tmp/folderlist.xml
sed -i -e "s/\$number/$number/" /tmp/folderlist.xml
sed -i -e "s/\$login/$login/" /tmp/folderlist.xml
sed -i -e "s/<\/folderlist>//" ~/.claws-mail/folderlist.xml
cat /tmp/folderlist.xml >> ~/.claws-mail/folderlist.xml
echo "</folderlist>" >> ~/.claws-mail/folderlist.xml

cp claws-mail/clawsrc ~/.claws-mail/


# chmod +x genpasswd
pass=`./genpasswd $password`
#gcc genpasswd.c -o genpasswd -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include -I/usr/lib/x86_64-linux-gnu/glib-2.0/include/ -lglib-2.0 -lgnutls

echo -e "[account:$number]\nrecv $pass" >> ~/.claws-mail/passwordstorerc
