#!/bin/bash
# ISP-A infra
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

# les scripts de créations de BAL pour clawsmail
cp -ar "clawsmail" /
chmod +x /clawsmail/addclawsuser.sh
chmod +x /clawsmail/genpasswd

#client pour tester la messagerie
DEBIAN_FRONTEND=noninteractive apt-get install -y claws-mail

# On configure les comptes mail
if [ -f /clawsmail/addclawsuser.sh ]; then
    su debian /clawsmail/addclawsuser.sh contact1 kaz.sns contact1@kaz.sns tototototo 1
    su debian /clawsmail/addclawsuser.sh contact2 kaz.sns contact2@kaz.sns tototototo 2
    su debian /clawsmail/addclawsuser.sh contact3 kaz.sns contact3@kaz.sns tototototo 3
    su debian /clawsmail/addclawsuser.sh contact4 kaz.sns contact4@kaz.sns tototototo 4
    su debian /clawsmail/addclawsuser.sh email isp-a.sns email email 5
fi

# On place les certifs
if  [ -f tls/root_ca.crt ]; then
  cp -ar tls/root_ca.crt /usr/local/share/ca-certificates/root.crt
  /usr/sbin/update-ca-certificates --fresh
fi

# Add to Firefox store
echo -e '{
 "policies": {
   "Certificates": {
      "ImportEnterpriseRoots": true,
      "Install": ["/etc/ssl/certs/root.pem"]
   }
 }
}' > /usr/lib/firefox-esr/distribution/policies.json
