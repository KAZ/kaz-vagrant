#!/bin/bash
# .sns registry

set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

DEB_VERSION=`cat /etc/debian_version | cut -d'.' -f1`
if [ $DEB_VERSION -eq "11" ] # DEB 11 aka Bullseye
then
	# disable systemd-resolved which conflicts with nsd
	echo "DNSStubListener=no" >> /etc/systemd/resolved.conf
	systemctl stop systemd-resolved
fi

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y nsd

echo -e "zone:
	name: \"sns.\"
	zonefile: \"sns.zone\"
" > /etc/nsd/nsd.conf

echo -e "\$TTL	86400
\$ORIGIN sns.
@  1D  IN  SOA ns.sns. hostmaster.sns. (
			      2002022401 ; serial
			      3H ; refresh
			      15 ; retry
			      1w ; expire
			      3h ; nxdomain ttl
			     )
       IN  NS     ns.sns.
ns    IN  A      100.100.20.10  ;name server definition
ns	IN	AAAA	2001:db8:a020::10
kaz.sns.		IN	NS	ns.kaz.sns.
ns.kaz.sns.	IN	A 100.80.0.2
isp-a.sns.	IN	NS	ns.isp-a.sns.
ns.isp-a.sns.	IN	A 100.120.1.2
ns.isp-a.sns.	IN	AAAA 2001:db8:120:1::2
mica.sns.	IN	NS	ns.mica.sns.
ns.mica.sns.	IN	A 100.82.0.2
ns.mica.sns.	IN	AAAA 2001:db8:82::2
" >> /etc/nsd/sns.zone
