#!/bin/bash
if [ -z "${SNSTERGUARD}" ] ; then
    exit 1
fi

DIR=$(cd "$(dirname $0)"; pwd)
cd "${DIR}"
set -e
export OUTPUT_DIR="/root/install"


mkdir -p "${OUTPUT_DIR}/log/"
export DebugLog="${OUTPUT_DIR}/log/log-kaz-$(date +%y-%m-%d-%T)-"
(
    echo "########## ********** Start kaz.sh $(date +%D-%T)"

    docker-clean -a
    rm -rf /kaz

    if [ -z "${KAZBRANCH}" ] ; then
	     KAZBRANCH="master"
    fi
    echo -e "\n    #### git checkout ${KAZBRANCH}\n"

    # copie des sources
    cd /
    git clone https://git.kaz.bzh/KAZ/KazV2.git kaz
    (cd /kaz ; git checkout "${KAZBRANCH}" )

    cp "${DIR}/kaz-config/dockers.env" /kaz/config/dockers.env
    for type in mail orga proxy withMail withoutMail ; do
	     [ -f "${DIR}/kaz-config/container-${type}.list" ] &&
       cp  "${DIR}/kaz-config/container-${type}.list" /kaz/config/
    done

    echo -e "\n    #### secretGen\n"
    /kaz/bin/secretGen.sh

    echo -e "\n    #### install\n"
	  /kaz/bin/install.sh

    # On crée quelques comptes
    mkdir -p /kaz/tmp
    cp /root/createUser.txt /kaz/tmp/
    /kaz/bin/createUser.sh -e || true

    # clear apt cache
    DEBIAN_FRONTEND=noninteractive apt-get autoremove -y
    DEBIAN_FRONTEND=noninteractive apt-get clean

    echo "########## ********** End kaz.sh $(date +%D-%T)"
)  > >(tee ${DebugLog}stdout.log) 2> >(tee ${DebugLog}stderr.log >&2)
