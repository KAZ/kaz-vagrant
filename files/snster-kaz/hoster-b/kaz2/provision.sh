#!/bin/bash
# Target DMZ
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

DEB_VERSION=`cat /etc/debian_version | cut -d'.' -f1`
if [ $DEB_VERSION -eq "11" ] # DEB 11 aka Bullseye
then
	# disable systemd-resolved which conflicts with nsd
	echo "DNSStubListener=no" >> /etc/systemd/resolved.conf
	systemctl stop systemd-resolved
fi

DEBIAN_FRONTEND=noninteractive apt-get update
DEBIAN_FRONTEND=noninteractive apt-get remove -y apache2
DEBIAN_FRONTEND=noninteractive apt-get autoremove -y


# Go KAZ !
# KAZ specific things
#installation de docker, docker-compose et on y fourre le user debian dans le groupe idoine

DEBIAN_FRONTEND=noninteractive apt-get install -y dos2unix jq ldapvi argon2 docker.io docker-clean git apg curl sudo unzip rsync btrfs-progs ldap-utils unaccent # fuse-overlayfs
usermod -G docker debian

# docker-compose
curl -SL https://github.com/docker/compose/releases/download/v2.17.3/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose

# activation dans alias dans /root/.bashrc
sed -i \
-e 's/^\# alias/alias/g' \
-e 's/^\# export/export/g' \
-e 's/^\# eval/eval/g' \
/root/.bashrc

if ! grep -q "for file in /dockers" /root/.bashrc 2>/dev/null; then
cat >> /root/.bashrc <<EOF
# enable bash completion in interactive shells
if ! shopt -oq posix; then
if [ -f /usr/share/bash-completion/bash_completion ]; then
. /usr/share/bash-completion/bash_completion
elif [ -f /etc/bash_completion ]; then
. /etc/bash_completion
fi
fi
for file in /kaz/bin/.*-completion.bash ; do
source "\${file}"
done
EOF
fi


# On met le GUARD pour la mise au point
echo "export SNSTERGUARD='true'" >> /root/.bashrc


# On place les certifs
cp -ar tls/root_ca.crt /usr/local/share/ca-certificates/
/usr/sbin/update-ca-certificates --fresh

mkdir -p /etc/letsencrypt/live/kaz.sns
cp tls/wildcard.crt /etc/letsencrypt/live/kaz.sns/fullchain.pem
cp tls/wildcard.key /etc/letsencrypt/live/kaz.sns/privkey.pem

# On sauve le proxy APT
proxy=$(/sbin/ip route | awk '/default/ { print $3 }' | head -1)
sed -i -e "s/^proxy.*$/proxy=$proxy/" /usr/local/sbin/detect_proxy.sh
#echo "export http_proxy=\"http://$proxy:3142\"" > /etc/profile.d/proxy.sh
#echo "export https_proxy=\"http://$proxy:3142\"" >> /etc/profile.d/proxy.sh

# Proxy pour les environnements durant les dockerbuilds
mkdir /root/.docker
echo "{
 \"proxies\":
 {
   \"default\":
   {
     \"httpProxy\": \"http://$proxy:3142\",
     \"httpsProxy\": \"http://$proxy:3142\",
     \"noProxy\": \"*.sns,127.0.0.1,localhost,127.0.0.0/8,100.64.0.0/10,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16,agora\"
   }
 }
}" > /root/.docker/config.json

# Proxy pour les docker pull -> commenté car pas de cache avec dockerhub
# echo "http_proxy=\"http://$proxy:3142\"
# https_proxy=\"http://$proxy:3142\"
# no_proxy=\"*.sns,127.0.0.0/8,100.64.0.0/10,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16\"
# " >> /etc/default/docker

# On active btrfs+registry miroir pour docker
cat >> /etc/docker/daemon.json <<EOF
{ "storage-driver": "btrfs",
"registry-mirrors": ["http://$proxy:5000"] }
EOF
service docker restart

# clear apt cache
DEBIAN_FRONTEND=noninteractive apt-get autoremove -y
DEBIAN_FRONTEND=noninteractive apt-get clean

# On installe Kaz
docker network create postfixNet # temp fix
ifconfig lo:0 100.81.0.2
echo "100.81.0.2 smtp imap www mail cloud cloud.kaz.sns tableur tableur.kaz.sns webmail webmail.kaz.sns garradin garradin.kaz.sns wiki wiki.kaz.sns git git.kaz.sns office office.kaz.sns depot depot.kaz.sns ldap ldap.kaz.sns mdp mdp.kaz.sns koffre koffre.kaz.sns pad pad.kaz.sns agora agora.kaz.sns dashboard dashboard.kaz.sns" >> /etc/hosts
cp "${DIR}/kaz.sh" /root/kaz.sh
cp -ar "${DIR}/kaz-config" /root/
chmod +x /root/kaz.sh
bash "/root/kaz.sh"
sed -i -e "s/100.81.0.2.*//g" /etc/hosts


# On démarre au boot
echo -e '#!/bin/sh\n/kaz/bin/container.sh start' >> /etc/rc.local
chmod +x /etc/rc.local


# notes fuse-overlayfs :
#mknod -m 666 /dev/fuse c 10 229
#echo -e '#!/bin/sh\nmknod -m 666 /dev/fuse c 10 229' >> /etc/rc.local
#chmod +x /etc/rc.local

# lxc.cgroup2.devices.allow = b 7:* rwm
# lxc.cgroup2.devices.allow = c 10:237 rwm
#
# mknod -m 666 /dev/loop0 b 7 0
# mknod -m 666 /dev/loop-control c 10 237
# truncate -s 30G /root/varlibdocker.img
# mkfs.btrfs /root/varlibdocker.img
# losetup -f /root/varlibdocker.img
# mount /dev/loop0 /var/lib/docker
