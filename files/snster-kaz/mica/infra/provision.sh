#!/bin/bash
# MICA infra
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

DEB_VERSION=`cat /etc/debian_version | cut -d'.' -f1`
if [ $DEB_VERSION -eq "11" ] # DEB 11 aka Bullseye
then
	# disable systemd-resolved which conflicts with nsd
	echo "DNSStubListener=no" >> /etc/systemd/resolved.conf
	systemctl stop systemd-resolved
fi

# manage mica.sns zone
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y unbound
cp dns.conf /etc/unbound/unbound.conf.d/

# On place les certifs
if  [ -f tls/root_ca.crt ]; then
  cp -ar tls/root_ca.crt /usr/local/share/ca-certificates/
  /usr/sbin/update-ca-certificates --fresh
fi

# Install smallstep CA / ACME server
cd /tmp
wget "https://dl.smallstep.com/gh-release/cli/gh-release-header/v0.24.4/step-cli_0.24.4_amd64.deb"
dpkg -i step-cli_0.24.4_amd64.deb
wget "https://dl.smallstep.com/gh-release/certificates/gh-release-header/v0.24.2/step-ca_0.24.2_amd64.deb"
dpkg -i step-ca_0.24.2_amd64.deb

echo "password" > /root/ca-passwordfile
step ca init --deployment-type=standalone --name="Kaz CA" --dns="ca.mica.sns" --acme --address=":443" --provisioner="contact@kaz.sns" --password-file="/root/ca-passwordfile" --root="${DIR}/tls/root_ca.crt" --key "${DIR}/tls/root_ca.key"
echo -e '#!/bin/sh\nstep-ca --password-file /root/ca-passwordfile' >> /etc/rc.local
chmod +x /etc/rc.local

# step ca init
# step ca root root.crt
# step ca provisioner add acme --type ACME
# certbot certonly -n --standalone -d www.target.sns   --server https://www.mica.sns/acme/acme/directory --agree-tos --email "fr@fr.fr"
