#!/bin/bash
set -e
if [ -z $SNSTERGUARD ] ; then exit 1; fi
DIR=`dirname $0`
cd `dirname $0`

mkdir -p /etc/unbound/unbound.conf.d/

cat >> /etc/unbound/unbound.conf.d/unblockdns.conf <<EOF
stub-zone:
  name: "sns"
  stub-addr: 100.100.20.10

stub-zone:
  name: "100.in-addr.arpa"
  stub-addr: 100.100.1.10

forward-zone:
  name: "."
  forward-addr: 100.64.0.1
EOF

# notes
# apt install build-essential libnghttp2-dev libssl-dev libexpat-dev
# wget https://nlnetlabs.nl/downloads/unbound/unbound-1.17.1.tar.gz
# ./configure --with-libnghttp2
# adduser unbound

# fixdns.sh
# SNSTER="snster -c /root/snster-kaz attach $1 -x"
# $SNSTER "DEBIAN_FRONTEND=noninteractive apt-get install -y  nss-tlsd libnss-tls"
# $SNSTER "sed -i -e 's/^hosts:\s*files/hosts:\tfiles tls/' /etc/nsswitch.conf"
