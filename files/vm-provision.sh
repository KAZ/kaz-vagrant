#!/bin/bash

if [ -z "${KAZGUARD}" ] ; then
    exit 1
fi

resize2fs /dev/sda1

DIR=$(cd "$(dirname $0)"; pwd)
cd "${DIR}"
set -e
export VAGRANT_SRC_DIR=/vagrant/files

mkdir -p "${VAGRANT_SRC_DIR}/log/"
export DebugLog="${VAGRANT_SRC_DIR}/log/log-vagrant-$(date +%y-%m-%d-%T)-"
(
    echo "########## ********** Start Vagrant $(date +%D-%T)"

    # Copie de qques fichiers
    cp "${VAGRANT_SRC_DIR}/keyboard" /etc/default/keyboard

    # gestions sources.list
    sed -i -e 's/main.*/main contrib non-free/' /etc/apt/sources.list
    sed -i -e 's/https:/http:/' /etc/apt/sources.list
    apt-get --allow-releaseinfo-change update

    # Cache APT
    #DEBIAN_FRONTEND=noninteractive apt-get install -y apt-cacher # apt-cacher-ng does not work well on bullseye
    #echo "allowed_hosts = *" >> /etc/apt-cacher/apt-cacher.conf
    #service apt-cacher restart
    DEBIAN_FRONTEND=noninteractive apt-get install -y squid
    sed -i -e "s/#http_access allow localnet/http_access allow localnet/" /etc/squid/squid.conf
    echo "cache_dir aufs /var/spool/squid 5000 14 256
maximum_object_size 4000 MB
http_port 3142" >> /etc/squid/squid.conf
    service squid restart
    echo "Acquire::http::Proxy \"http://127.0.0.1:3142\";" > /etc/apt/apt.conf.d/01proxy;  # utilisation de apt-cacher-ng
    # Ajouter http://www.squid-cache.org/Doc/config/cache_peer/ à squid pour un proxy upstream

    # Pour le confort de chacun, un customVM.sh optionnel
    if [ -f "${VAGRANT_SRC_DIR}/customVM.sh" ]; then
	     bash "${VAGRANT_SRC_DIR}/customVM.sh"
    fi

    # MAJ et Install
    DEBIAN_FRONTEND=noninteractive apt-get -y upgrade
    DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade
    DEBIAN_FRONTEND=noninteractive apt-get install -y linux-headers-`dpkg --print-architecture` virtualbox-guest-additions-iso dkms apg curl git sudo unzip rsync firefox-esr tcpdump net-tools mousepad wireshark swapspace whois python3-lxc lxc python3-pygraphviz python3-pil python3-yaml imagemagick btrfs-progs docker-registry # could be with --no-install-recommends
    DEBIAN_FRONTEND=noninteractive apt-get install -y xfce4 lightdm xfce4-terminal xserver-xorg gitk # needs to install recommends

    # vbox guest utils
    #VERSION=`curl https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT`
    #curl https://download.virtualbox.org/virtualbox/$VERSION/VBoxGuestAdditions_$VERSION.iso -o /tmp/vbox.iso
    #mount -o loop /tmp/vbox.iso /mnt
    mount -o loop /usr/share/virtualbox/VBoxGuestAdditions.iso /mnt
    /mnt/VBoxLinuxAdditions.run || true # vboxsf module will fail to load before reboot, expected behavior
    /sbin/rcvboxadd quicksetup all || true
    umount /mnt

    ssh-keygen -t rsa -b 4096 -N '' <<<$'\ny'
    rsync /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys

    # Localisation du $LANG, en par défaut, timezone Paris
    if [ -z "${HOSTLANG}" ] ; then
	     HOSTLANG="en_US.UTF-8"
    fi
    echo "Europe/Paris" > /etc/timezone
    ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
    dpkg-reconfigure -f noninteractive tzdata
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
    sed -i -e "s/# ${HOSTLANG} /${HOSTLANG} /" /etc/locale.gen
    echo "LANG=\"${HOSTLANG}\"">/etc/default/locale
    dpkg-reconfigure --frontend=noninteractive locales || true  # don't fail for a locales problem
    update-locale LANG=${HOSTLANG} || true   # don't fail for a locales problem

    # Creation des utilisateurs
    echo -e "\n    #### create user\n"
    usermod -p $(mkpasswd --method=sha-512 root) root
    useradd -m -s "/bin/bash" -p $(mkpasswd --method=sha-512 debian) debian || true # don't fail if user already exists

    # Désactivation de la mise en veille de l'écran
    mkdir -p /etc/X11/xorg.conf.d/
    rsync -a "${VAGRANT_SRC_DIR}/10-monitor.conf" /etc/X11/xorg.conf.d/
    # mv /etc/xdg/autostart/light-locker.desktop /etc/xdg/autostart/light-locker.desktop.bak
    DEBIAN_FRONTEND=noninteractive apt-get remove --purge -y light-locker

    #faut virer exim, inutile
    DEBIAN_FRONTEND=noninteractive apt-get remove --purge -y exim4-base exim4-config exim4-daemon-light

    #login ssh avec mot de passe
    sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/" /etc/ssh/sshd_config
    if ! grep -q "PasswordAuthentication yes" /etc/ssh/sshd_config 2>/dev/null; then
	     echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
    fi

    # autorisation du routing et augmentation inotify
    if ! grep -q "net.ipv4.ip_forward" /etc/sysctl.conf 2>/dev/null; then
	     echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
    fi
    sed -i "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/" /etc/sysctl.conf
    if ! grep -q "fs.inotify.max_queued_events" /etc/sysctl.conf 2>/dev/null; then
	     echo -e "fs.inotify.max_queued_events=1048576\nfs.inotify.max_user_instances=1048576\nfs.inotify.max_user_watches=1048576" >> /etc/sysctl.conf
    fi
    sysctl -p


    # enable bash autocompletion
	cat >> /etc/bash.bashrc <<EOF
# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
EOF

    # XFCE4 panel: use default config
    # source: https://forum.xfce.org/viewtopic.php?pid=36585#p36585
    rsync -a /etc/xdg/xfce4/panel/default.xml /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

    # Permetre l'édition emacs des lignes de commande (exemple "Esc. flèche gauche" pour "déplace d'un mot à gauche")
    TERM_CFG=/root/.config/xfce4/terminal/terminalrc
    mkdir -p $(dirname "${TERM_CFG}")
    touch "${TERM_CFG}"
    if ! grep -q "ShortcutsNoMnemonics" "${TERM_CFG}" 2>/dev/null; then
	     echo -e "[Configuration]\nShortcutsNoMnemonics=TRUE" >> "${TERM_CFG}"
    fi

    # free swapspace at shutdown
    echo -e "\n    #### set swapspace\n"
    sed -i -e 's/ExecStart=\/usr\/sbin\/swapspace/ExecStart=\/usr\/sbin\/swapspace\nExecStop=\/usr\/sbin\/swapspace -e/' /lib/systemd/system/swapspace.service
    systemctl daemon-reload

    # limit journald log size
    mkdir -p /etc/systemd/journald.conf.d
    if [ ! -f /etc/systemd/journald.conf.d/sizelimit.conf ]; then
	     cat > /etc/systemd/journald.conf.d/sizelimit.conf <<EOF
[Journal]
SystemMaxUse=20M
SystemMaxFileSize=2M
EOF
    fi

    # CA et certifs avec smallstep

    echo -e "\n    #### smallstep\n"
    cd /tmp
    wget "https://dl.smallstep.com/gh-release/cli/gh-release-header/v0.24.4/step-cli_0.24.4_amd64.deb"
    dpkg -i step-cli_0.24.4_amd64.deb
    echo "password" > /root/ca-passwordfile
    step ca init --deployment-type=standalone --name="Kaz CA" --dns="ca.mica.sns" --acme --address=":443" --provisioner="contact@kaz.sns" --password-file="/root/ca-passwordfile"

    mkdir -p /root/tls
    cp /root/.step/certs/root_ca.crt /root/tls/root_ca.crt
    chmod 644 /root/tls/root_ca.crt
    cp /root/.step/secrets/root_ca_key /root/tls/root_ca.key
    step crypto change-pass /root/tls/root_ca.key --no-password --insecure --password-file="/root/ca-passwordfile" --force

    step certificate create "*.kaz.sns" /root/tls/wildcard.crt /root/tls/wildcard.key --profile leaf --ca /root/.step/certs/intermediate_ca.crt --ca-key /root/.step/secrets/intermediate_ca_key --ca-password-file /root/ca-passwordfile --bundle --force --no-password --insecure --not-after 20000h

    # Cache docker registry
    echo "proxy:
  remoteurl: https://registry-1.docker.io
auth:
  none:
" >> /etc/docker/registry/config.yml
    service docker-registry restart

    # clear apt cache
    DEBIAN_FRONTEND=noninteractive apt-get autoremove -y
    DEBIAN_FRONTEND=noninteractive apt-get clean

    # SNSTER
    cd
    git clone https://framagit.org/flesueur/snster.git
    cd snster
    # git checkout tags/v1.1.0
    git checkout 34b7c57cb7480cb4b3a84d14a1a46b60d0eb5967
    ./install.sh

    # BTRFS avec hotfix sale de SNSTER
    freespace=`df /root | awk '/[0-9]%/{print $(NF-2)}'`
    btrsize=$(( $freespace - 5000000 )) # on laisse 5GB libres
    truncate -s ${btrsize}k /root/btrfs.img
    mkfs.btrfs -f /root/btrfs.img
    echo "/root/btrfs.img	/var/lib/lxc	btrfs	loop	0	0" >> /etc/fstab
    mount /var/lib/lxc
    sed -i -e "s/template=self.template/template=self.template, bdevtype='btrfs'/" /usr/local/lib/python3.11/dist-packages/backends/LxcBackend.py

    # SNSTER KAZ
    cp -ar ${VAGRANT_SRC_DIR}/snster-kaz /root

    # crypto keys
    cp -ar /root/tls /root/snster-kaz/hoster-a/kaz1/
    cp -ar /root/tls /root/snster-kaz/hoster-b/kaz2/
    cp -ar /root/tls /root/snster-kaz/isp-a/home/
    cp -ar /root/tls /root/snster-kaz/mica/infra/


    # On lie le filesystem de kaz-prod dans le /kaz de la VM pour le dév
    ln -s /var/lib/lxc/kaz-hoster-a-kaz1/rootfs/ /kaz1-prod
    ln -s /var/lib/lxc/kaz-hoster-b-kaz2/rootfs/ /kaz2-prod
    ln -s /kaz-prod1/kaz /kaz

    # On met le KAZGUARD pour la mise au point
    echo "export KAZGUARD='true'" >> /root/.bashrc

    # quelques détails
    cp "${VAGRANT_SRC_DIR}/vm-upgrade.sh" /root/
    chmod +x /root/vm-upgrade.sh

    echo -e '#!/bin/sh\nsnster -c /root/snster-kaz start' >> /etc/rc.local
    chmod +x /etc/rc.local

    # Build SNSTER KAZ !
    snster -c /root/snster-kaz create

    echo "########## ********** End Vagrant $(date +%D-%T)"
)  > >(tee ${DebugLog}stdout.log) 2> >(tee ${DebugLog}stderr.log >&2)

reboot

# Pour sympa-SOAP
# KAZPROD="snster -c /root/snster-kaz -t /root/templates attach kaz-prod -x"
# ${KAZPROD} "docker cp /etc/letsencrypt/local/rootCA.pem sympaServ:/usr/local/share/ca-certificates/rootCA.crt"
# ${KAZPROD} "docker exec -it sympaServ update-ca-certificates"

# Interception https avec squid-openssl (nok pour dockerhub) :
# http_port 3142 tcpkeepalive=60,30,3 ssl-bump generate-host-certificates=on dynamic_cert_mem_cache_size=20MB tls-cert=/etc/letsencrypt/local/rootCA.pem tls-key=/etc/letsencrypt/local/rootCA-key.pem tls-dh=prime256v1:/etc/letsencrypt/local/dhparam.pem
# sslcrtd_program /usr/lib/squid/security_file_certgen -s /var/lib/ssl_db -M 4MB
# sslcrtd_children 5
# ssl_bump server-first all
# ssl_bump stare all
# sslproxy_cert_error deny all
# refresh_pattern -i (/blobs/sha256)     1440 99% 10080  ignore-no-store ignore-private override-expire store-stale reload-into-ims
# refresh_pattern -i (/images/sha256)    1440 99% 10080  ignore-no-store ignore-private override-expire store-stale reload-into-ims
# refresh_pattern -i (/manifests/)       1440 99% 10080  ignore-no-store ignore-private override-expire store-stale reload-into-ims
